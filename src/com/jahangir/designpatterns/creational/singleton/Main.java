package com.jahangir.designpatterns.creational.singleton;

public class Main {

	public static void main(String[] args) {
		
		Singleton singleton = Singleton.getSingletonInstance();
		System.out.println(singleton.getTitle());
		singleton.setTitle("New Title");
		System.out.println(singleton.getTitle());
		Singleton singletonTwo = Singleton.getSingletonInstance();
		System.out.println(singletonTwo.getTitle());
	}

}
