package com.jahangir.designpatterns.creational.singleton;

public class Singleton {
	private static Singleton singleton;
	private String title;

	private Singleton() {
		this.title = "Singleton object is instanciated";
	}

	public static Singleton getSingletonInstance() {
		if (singleton == null) {
			singleton = new Singleton();
		}
		return singleton;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
